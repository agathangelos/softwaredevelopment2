import unittest
from game import*

class TestStringMethods(unittest.TestCase):
    testID = 0

    def test_Card_Class(self):
        card = Card(name='Archer', attack=3, money=0, cost=2)
        card2 = Card(name='Archer', attack=3, money=0, cost=2)
        cardTest1 = self.assertEqual(card.card_string(), \
'Archer          2               3               0')
        cardTest2 = self.assertEqual(card.get_attack(), 3)
        cardTest3 = self.assertEqual(card.get_money(), 0)
        cardTest4 = self.assertEqual(card.get_cost(), 2)
        cardTest5 = self.assertEqual(card, card2)
        self.testID = self.testID + 1
        if (cardTest1 == cardTest2 == cardTest3 == cardTest4 == None):
            print "\\> %d: Card class pass the Test" % self.testID

    def test_PlayerClass(self):
        central_deck, human_player, computer_player = game_initialize()
        #manualy initialize deck for comaprison
        initial_deck = [8 * [Card('Serf', 0, 1, 0)],
                        2 * [Card('Squire', 1, 0, 0)]
                       ]
        initial_deck = list(itertools.chain.from_iterable(initial_deck))

        #test if human initial deck is ok
        playerTest1 = self.assertEqual(human_player.deck, initial_deck)
        
        #add all the cards at human player hand
        pop_multiple_elements(human_player.deck, human_player.hand, 10)
        
        #move playes cards at the active space and test if 
        # a. they are there
        # b. the players money
        # c. the players attack value
        
        human_player.take_cart(0, 9)
        playerTest2 = self.assertEqual(human_player.money, 8)
        playerTest3 = self.assertEqual(human_player.attack, 2)

        #test attack function
        #human attacks on computer
        human_player.attack_operation(computer_player)

        playerTest4 = self.assertEqual(human_player.attack, 0)
        playerTest5 = self.assertEqual(computer_player.health, (30-2))
        
        human_player.end_turn()
        playerTest6 = self.assertEqual(len(human_player.hand), 5)
        playerTest7 = self.assertEqual(len(human_player.active), 0)
        playerTest8 = self.assertEqual(human_player.attack, 0)
        playerTest9 = self.assertEqual(human_player.money, 0)

        self.testID = self.testID + 1
        if (playerTest1 == playerTest2 == playerTest3 == \
            playerTest4 == playerTest5 == playerTest6 == 
            playerTest7 == playerTest8 == playerTest9 == None):
            print "\\> %d: Player class pass the Test" % self.testID
        
    check_for_win(human_player, computer_player, central_deck)  # TODO CHECK it

if __name__ == '__main__':
    unittest.main()
    
