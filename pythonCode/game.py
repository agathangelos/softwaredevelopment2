"""
This Program inplements a Card Building Game
"""
import itertools
import random
import os
import sys
import getopt
import curses
from enum import Enum

class Orientation(Enum):
    """
    Enumeration Class help to descripe the way a panel is
    printed at screen
    For example, if is needed to print the numbers from 1 to 5 with
    horizontal or vertical orientetion it will be like
    the following examples respectivly:

    Horizontal means that the panel is printed like
    1 2 3 4 5

    Vertical means that the panel is printed like
    1
    2
    3
    4
    5
    """
    horizontal = 0
    vertical = 1

class GameDifficulty(Enum):
    """
    Enumeration Class help to descripe the opponents play style.

    When the oponents plays aggresive its buys the cards with the
    higher strong value.

    When the oponents plays acquisitive its buys the cards with the
    higher money value.
    """
    aggressive = 0
    acquisitive = 1

    def reverse(self, strategy):
        """
        Returns the oposite strategy
        """
        if strategy == GameDifficulty.aggressive:
            return GameDifficulty.acquisitive
        if strategy == GameDifficulty.acquisitive:
            return GameDifficulty.aggressive

class Card(object):
    """
    The Card Class is responsiple to represend all cards of the game.
    Contains:
        card name
        cart attack force
        card sell money value
        card cost to buy
    """
    def __init__(self, name, attack, money, cost):
        """
        Responsiple to initialise Card class fields

        name -- the name of the card
        attack -- the attack strong of the card
        money -- the money that the card is provoide
        cost -- cost to buy the card
        """
        self.name = name
        self.attack = attack
        self.money = money
        self.cost = cost

    def __str__(self):
        """
        Returning a string with all of the class fields ready to presend
        at the screen
        """
        return "%-16s%-16s%-16s%-s" % (self.name, self.cost,
                                       self.attack, self.money)

    def get_attack(self):
        """
        Return the attack value of the card
        """
        return self.attack

    def get_money(self):
        """
        Return the money value of the card
        """
        return self.money

    def get_cost(self):
        """
        Return the cost value of the card
        """
        return self.cost

    def __eq__(self, other):
        """
        Compares Card objects and if they have the same values
        on their coresponting field that function retuns true
        else return false
        """
        return (self.name == other.name) or \
               (self.cost == other.cost) or \
               (self.attack == other.attack) or \
               (self.money == other.money)

class Player(object):
    """
    The Player Class is represending the players bouth human and computer.
    It is responsiple for every Player operetion.
    Player operetions are buy card, take cards from the hand and
    place them at the active cards space.
    It is also responsible to provide any needed information about
    the player, like the name, health, player deck,
    player hand, player active cards, player number of cards,
    discard cards, money, attack power and boughtCards during the round
    """

    def __init__(self, deck, hand, active, \
                 discard, boughtCards, handsize=5, health=30):
        """
        Initialise the Player class. If no values are given to the
        initialization fuction, there are plyselected values.

        health -- health level of the player
        deck -- players deck
        hand -- players hand
        active -- player active cards
        handsie -- player hand size
        discard -- discard card
        """
        self.health = health
        self.deck = deck
        self.hand = hand
        self.active = active
        self.handsize = handsize
        self.discard = discard
        self.boughtCards = boughtCards
        self.money = 0
        self.attack = 0
        self.strategy = 0

    def set_strategy(self, strategy_style):
        """
        If the player is not human this function is setting up his strategy
        """
        self.strategy = strategy_style

    def take_cart(self, start_index, end_index):
        """
        Is taking cards from player hand and playsing
        them at players active space.
        Player hand and player active are represended by list.

        start_index -- the start index from where the copy is starting
        end_index -- the index where the transefing will stop
        """
        for card_index in range(end_index, start_index-1, -1):
            card = self.hand.pop(card_index)
            self.money = self.money + card.get_money()
            self.attack = self.attack + card.get_attack()
            self.active.append(card)

    def attack_operation(self, opponent):
        """
        The player is attaking to the opponent
        """
        opponent.health = opponent.health - self.attack
        self.attack = 0

    def end_turn(self):
        """
        Handle the player turn termination.
        When that function returns, the player is ending its turn.
        """
        counter = len(self.hand)
        while counter > 0:
            self.discard.append(self.hand.pop())
            counter = counter - 1

        counter = len(self.active)
        while counter > 0:
            self.discard.append(self.active.pop())
            counter = counter - 1

        counter = self.handsize
        while counter > 0:
            if len(self.deck) == 0:
                random.shuffle(self.discard)
                self.deck = self.discard
                self.discard = []
            card = self.deck.pop()
            self.hand.append(card)
            counter = counter - 1

        self.money = 0
        self.attack = 0

    def buy_card(self, consol, bought_cards_panel, availiable_cards_panel,
                 supplement_cards_panel, status_money_attack_panel, central):
        """
        Is responsible to hundle the players buy procedure. And display
        the "Buy Menu" at screen.

        consol -- object of class Consol. This method it taking the class
                  object and using it to display its information at the screen.

        bought_cards_panel -- object of class Panel. It is responsible to presend
                            the buy menu panel.

        availiable_cards_panel -- object of class Panel. It is displaying the
                                available cards for purchase
        supplement_cards_panel -- object of class Panel. If there is a supplement
                                card that object is responsible to display it.
        status_money_attack_panel -- Is the panel that displays players money and
                                  attack status.
        central -- central is a dictionary represents all the cards that are at
                   the common area.

        Inner Operations:
        Until the call at consol.draw_consol( ) there are initializations of the
        screen and when the function is called is drawing the screen.
        consol.draw_consol( ) is waiting for the user to make a choose. The user
        choose is returned at the "select" variable.

        The user options are listed (enumerated) vertically counting from one.
        The last option is the "End Buying".
        When a user selects a card the card is transmited from its initial
        place at the "Bought Cards" panel and the players money are reduce
        depenting on the bought card cost.
        """
        # there is no available card to buy from the central deck and the game
        # has to exit
        if not central['active']:
            return

        boughtCards = []
        while True:
            availiable_cards_panel.set_message(central['active'])
            if central['supplement']:
                supplement_cards_panel.set_message([central['supplement'][0]])
            else:
                supplement_cards_panel.set_message([])

            status_money_attack_panel.set_message([self.money, self.attack])
            bought_cards_panel.set_message(boughtCards)

            option_list = []
            for index, card in enumerate(central['active']):
                option_list.append("Buy " + card.name +
                                   " card(" + str(index+1) + ")")

            option_list.append("Buy Supplement card")
            option_list.append("End Buying")
            consol.read_input("Choose Action:", option_list)

            select = consol.draw_consol([availiable_cards_panel,
                                         supplement_cards_panel,
                                         bought_cards_panel,
                                         status_money_attack_panel], 
                                         right_panel=None, health_status=None)

            card_index = len(central['active'])+1
            supplement_index = card_index
            end_index = supplement_index + 1

            if select < card_index:
                index = select - 1
                if self.money >= central['active'][index].cost:
                    self.money = self.money - central['active'][index].cost
                    self.discard.append(central['active'].pop(index))
                    boughtCards.append(self.discard[-1])
                    if len(central['deck']) > 0:
                        card = central['deck'].pop()
                        central['active'].append(card)
                    else:
                        central['activeSize'] = central['activeSize'] - 1
            elif select == supplement_index:
                if central['supplement']:
                    if self.money >= central['supplement'][0].cost:
                        self.money = self.money - central['supplement'][0].cost
                        self.discard.append(central['supplement'].pop())
                        boughtCards.append(self.discard[-1])
            elif select == end_index:
                break

    def auto_buy_cards(self, central, level=0):
        """
        This function is responsible for the computers strategy on buying
        cards. It is inplementing the knap sack 01 algorithm but on each
        iteration its picking only one of the sack items. That is happening
        becaouse when the player takes one card from the availiable cards a
        new card appears and we need the player to make the best choice.

        strategy - it is determians if the player is following aggressive or
                   acquisitive game static.
        central - cetral is the common area between the two players and from
                  here the auto_buy_cards function have access at the available
                  cards and the available supplement card.

        return
        It is returning 'true' if the player had bought a card and 'false' if
        he had not.
        """

        # there is no available card to buy from the central deck and the game
        # has to exit
        if not central['active']:
            return

        weight = self.money
        cardss = list(central['active'])

        if central['supplement']:
            cardss.append(central['supplement'][0])

        set_size = len(cardss)
        array = [[0 for x in range(weight+1)] for x in range(set_size+1)]   #init to zero

        cardss.sort(key=lambda x: x.cost)
        # Build table array[][] in bottom up manner
        for i in range(1, set_size+1):
            for j in range(1, weight+1):
                if cardss[i-1].cost <= j:
                    if self.strategy == GameDifficulty.aggressive:
                        array[i][j] = max(array[i-1][j], cardss[i-1].attack +
                                          array[i-1][j-cardss[i-1].cost])
                    else:
                        array[i][j] = max(array[i-1][j], cardss[i-1].money +
                                          array[i-1][j-cardss[i-1].cost])
                else:
                    array[i][j] = array[i-1][j]

        #find K-NapSack results
        actual_cards = []
        i = set_size
        j = weight
        while (i > 0) and (j > 0):
            if array[i][j] != array[i-1][j]:
                actual_cards.append(cardss[i-1])
                i = i - 1
                j = j - cardss[i].cost
            else:
                i = i - 1

        availiable_cards = list(actual_cards)
#        availiable_cards = list(central['active'])
        if self.strategy == GameDifficulty.aggressive:
            availiable_cards.sort(key=lambda x: x.attack, reverse=True)
        else:
            availiable_cards.sort(key=lambda x: x.money, reverse=True)

        can_buy_cards = []
        for i, item in enumerate(availiable_cards):
            if item.cost <= self.money:
                can_buy_cards.append(item)

        if not can_buy_cards:
            if self.money > 0 and level == 0:
                self.strategy.reverse(self.strategy)
                result = self.auto_buy_cards(central, 1)
                self.strategy.reverse(self.strategy)
                return result
            else:
                return False

        card_name = can_buy_cards[0].name
        if card_name == "Levy":
            self.discard.append(central['supplement'].pop())
            self.boughtCards.append(self.discard[-1])
        else:
            for i, item in enumerate(central['active']):
                if item.name == card_name:
                    self.discard.append(central['active'].pop(i))
                    self.boughtCards.append(self.discard[-1])
                    if len(central['deck']) > 0:
                        card = central['deck'].pop()
                        central['active'].append(card)
                    break

        self.money = self.money - self.boughtCards[-1].cost
        return True

class Panel(object):
    """
    Panel Class

    """
    def __init__(self, screen, title, description,
                 orientation=Orientation.vertical):
        """
        Initialize the Panel object mebers

        screen - screen is an object of class Consol. Is used by the class
                 object to communicate with the screen. It is printing on the
                 using that Consol object.
        title - title is the title of the panel. It is printed at the top of
                the panel.
        description - Represends the labels of the panel
        dynamic_message - What ever is in dynamic_message list will be displayed
                         by the panel
        orientation - the orientation determins if the panel is going to be
                      printed vertically or horizontally
        """
        self.screen = screen
        self.title = title
        self.description = description
        self.dynamic_message = []
        self.orientation = orientation

    def set_message(self, msg):
        """
        Is setting the value of dynamic Message
        What ever is in dynamic_message list will be displayed by the panel
        """
        self.dynamic_message = msg

    def draw(self, row, col, colors):
        """
        The Consol object who is responsible to draw the screen. When is needed
        to draw a panel is calling that function of the panel to draw it.

        row - the row of the screen where the panel will start printed
        col - the column of the screen where the panel will start printed
        """
        row = row + 1
        if self.orientation == Orientation.vertical:
            self.screen.addstr(row, col, self.title, curses.A_BOLD | colors)
            row = row + 1
            self.screen.addstr(row, col, self.description, curses.A_BOLD | \
                                                           curses.A_UNDERLINE | colors)
            row = row + 1
            if len(self.dynamic_message) == 1:
                self.screen.addstr(row, 2, "  | " +
                                   self.dynamic_message[0].__str__(),
                                   curses.A_BLINK)
                row = row + 1
            elif len(self.dynamic_message) == 0:
                self.screen.addstr(row, 2, "  | No cards", curses.A_BLINK)
                row = row + 1
            else:
                for item, message in enumerate(self.dynamic_message):
                    self.screen.addstr(row, 2, str(item+1) + " | " +
                                       message.__str__(),
                                       curses.A_BLINK)
                    row = row + 1
        else:
            if (row < 38) and (col < 150):
                self.screen.addstr(row, col, self.title, curses.A_BOLD | colors)
                row = row + 1
                for key, msg in enumerate(self.description):
                    string = self.description[key] + \
                             str(self.dynamic_message[key])
                    if (row < 38) and (col < 150):
                        self.screen.addstr(row, col, string, curses.A_BLINK)
                        row = row + 1

        return (row, col)

class Consol(object):
    """
    There is one object of that class at the program and is repsonsible for
    everything that is going to be printed at the screen.
    """
    # assume_default_colors
    def __init__(self, brightnes):
        """
        screen -
        msg -
        options -
        brightnes -
        """
        self.screen = curses.initscr()
        self.msg = ""
        self.options = []
        if brightnes == "bl" or brightnes == "black":
            self.fg = 254
            self.bg = -1
        else:
            self.fg = -1
            self.bg = -1

    def read_input(self, title, options):
        """
        This function is responsible for the part of the screen that is
        detecate to help the user express his choice. It is setting up the
        title of the input panel, and the options list, from where the user
        will make his choices. When the drawConsol function is call is drawing
        the "msg" and the "options" list.

        msg - the title of the input panel
        options - the availiable choices for the user
        """
        self.msg = title
        self.options = options

    def draw_consol(self, left_panels=None, right_panel=None, health_status=None):
        """
        This function is responsible to draw the consol

        left_panels - A list of the panels that have to be draw on
                      the left of the screen.
        right_panel - A list of the panels that are going to be darw on the
                     right of the panels that draw by the "panel list"
        health_status - Health panel is the panel who is responsible to presend
                      to the user the health status of the game players.
        """
        # Disables automatic echoing of key presses (prevents program from
        # input each key twice)
        curses.noecho()
        # Disables line buffering (runs each key as it is pressed rather than
        # waiting for the return key to pressed)
        curses.cbreak()
        # Lets you use colors when highlighting selected menu option
        curses.start_color()

        #added

        curses.use_default_colors()

        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)

        # -1 color is the default terminal color
        curses.init_pair(2, self.fg, self.bg)

        self.screen.keypad(1)

        row = 2
        pos = 0
        user_input = None
        # I'm going to be lazy and save some typing here.
        pair_red_white = curses.color_pair(1)
        colors = curses.color_pair(2)
        normal = curses.A_NORMAL
        self.screen.resize(34, 148)
        while user_input != ord('\n'):
            self.screen.clear()
            self.screen.border(0)
            row = 0
            self.screen.addstr(row, 2, " Deck Building Card Game ",
                               curses.A_STANDOUT | curses.A_BOLD | curses.color_pair(2))
            row = row + 1

            col = 2
            for panel in left_panels:
                (row, col) = panel.draw(row, col, colors)

            if right_panel:
                row = 1 + 1
                col = 70
                for panel in right_panel:
                    (row, col) = panel.draw(row, col, colors)
                row = row + 2
                self.screen.addstr(row, col, self.msg, curses.A_BOLD | curses.color_pair(2))
                row = row + 2
            else:
                row = 1 + 1
                col = 70 # 2
                self.screen.addstr(row, col, self.msg, curses.A_BOLD | curses.color_pair(2))
                row = row + 2

            if health_status:
#                curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_RED)
#                curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_RED)
                self.screen.addstr(1, 104, "+--------------------------------\
--------+", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(2, 106, "Player Health", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(2, 127, "                 ",
                                   curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(5, 106, "Computer Player Heath",
                                   curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(5, 135, "         ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(7, 104, "+--------------------------------\
--------+", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(2, 104, "| ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(3, 104, "| ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(4, 104, "| ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(5, 104, "| ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(6, 104, "| ", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(2, 144, " |", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(3, 144, " |", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(4, 144, " |", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(5, 144, " |", curses.A_BOLD | curses.color_pair(2))
                self.screen.addstr(6, 144, " |", curses.A_BOLD | curses.color_pair(2))

                diff = 30 - health_status[0]
                i = 0
                for i in range(0, health_status[0]):
                    self.screen.addstr(3, 106+diff+i, "*", curses.A_BOLD | curses.color_pair(2))

                self.screen.addstr(3, 109+29, str(health_status[0])+
                                   str("/30"), curses.A_BOLD | curses.color_pair(2))

                diff = 30 - health_status[1]
                for i in range(0, health_status[1]):
                    self.screen.addstr(6, 106+diff+i, "*", curses.A_BOLD | curses.color_pair(2))

                self.screen.addstr(6, 109+29, str(health_status[1])+
                                   str("/30"), curses.A_BOLD | curses.color_pair(2))

            string = ""
            for i, opt in enumerate(self.options):
                num = str(0)
                if (i+1) < 10:
                    num = str(i+1) + " "
                else:
                    num = str(i+1)

                if isinstance(opt, int):
                    string = num + " - Select Card " + str(opt)
                else:
                    string = num+" - "+opt
                if pos == i:
                    self.screen.addstr(row, col+2, string, pair_red_white)
                    # may change to curses.color_pair(2)
                else:
                    self.screen.addstr(row, col+2, string, normal)
                row = row + 1

            self.screen.addstr(row, col+2, "", normal)
            row = row + 1

            max_index = len(self.options) # - 1

            self.screen.refresh()
            user_input = self.screen.getch()

            if user_input >= 49 and user_input < (len(self.options)+49):
                for i in range(0, len(self.options)):
                    if user_input == i+49:
                        pos = user_input-49
            elif user_input == curses.KEY_DOWN: #258:
                if pos < (max_index-1):
                    pos += 1
                else:
                    pos = 0 #max_index
            elif user_input == curses.KEY_UP: #259:
                if pos > 0:
                    pos += -1
                else:
                    pos = max_index - 1 #0

        self.msg = ""
        self.options = []
        return pos+1

    def msg_print(self, string):
        """
        This function is used for dubugging perpuses, and is printing messages
        on screen.
        """
        self.screen.addstr(40, 2, string, curses.A_BLINK)

def pop_multiple_elements(from_list, to_list, num_of_elements):
    """
    Pops multiple elements for a list

    from_list -- Targeting list
    to_list -- Destination list
    num_of_elements -- The number of the elements that are going to be pop
    """
    counter = num_of_elements
    while counter > 0:
        element = from_list.pop()
        to_list.append(element)
        counter = counter - 1

def game_initialize():
    """This function is responsible to initialize the game.

    Return:
    central_deck -- Is representing the common areas of the two players.
    suits -- There are all of the game available cards
    initial_deck -- The initial deck of the player
    supplement -- The cards at the supplement deck
    """
    central_deck = {'active': [],
                    'activeSize': 5,
                    'supplement': [],
                    'deck': []
                   }

    suits = [4 * [Card('Archer', 3, 0, 2)],
             4 * [Card('Baker', 0, 3, 2)],
             3 * [Card('Swordsman', 4, 0, 3)],
             2 * [Card('Knight', 6, 0, 5)],
             3 * [Card('Tailor', 0, 4, 3)],
             3 * [Card('Crossbowman', 4, 0, 3)],
             3 * [Card('Merchant', 0, 5, 4)],
             4 * [Card('Thug', 2, 0, 1)],
             4 * [Card('Thief', 1, 1, 1)],
             2 * [Card('Catapault', 7, 0, 6)],
             2 * [Card('Caravan', 1, 5, 5)],
             2 * [Card('Assassin', 5, 0, 4)]
            ]

    initial_deck = [8 * [Card('Serf', 0, 1, 0)],
                    2 * [Card('Squire', 1, 0, 0)]
                   ]

    supplement = 10 * [Card('Levy', 1, 2, 2)]

    deck = list(itertools.chain.from_iterable(suits))
    random.shuffle(deck)
    central_deck['deck'] = deck
    central_deck['supplement'] = supplement
    central_deck['active'] = []

    human_player = Player(deck=list(itertools.chain.from_iterable(initial_deck)),
                          hand=[], active=[], discard=[], boughtCards=[],
                          handsize=5, health=30)

    computer_player = Player(deck=list(itertools.chain.from_iterable(initial_deck)),
                             hand=[], active=[], discard=[], boughtCards=[],
                             handsize=5, health=30)

    return (central_deck, human_player, computer_player)

def check_for_win(human_player, computer_player, central_deck):
    """
    This function determins if the game is end and the game result.
    The game can end eather with one of the players win, or with draw.
    """
    if human_player.health <= 0:
        return "Computer wins"
    elif computer_player.health <= 0:
        return 'Player One Wins'
    elif not central_deck['active']:
#        print "No more cards available"
        if human_player.health > computer_player.health:
            return "Player One Wins on Health (No more cards available)"
        elif computer_player.health > human_player.health:
            return "Computer Wins on Health (No more cards available)"
        else:
            return "Draw"
    else: #how to handle this? Which attack power to count???
#        pHT = 0
#        pCT = 0
#        if pHT > pCT:
#            print "Player One Wins on Card Strength"
#        elif pCT > pHT:
#            print "Computer Wins on Card Strength"
#        else:
        return None


def main(brightnes):
    """
    This is te main function of the game and is responsible to orchistrate 
    the game functionalities 
    """
    consol = Consol(brightnes)
    play_the_game = True
    result = None

    # if play_the_game is true the player will start playing a new game
    while play_the_game:
        #make game initializations
        central_deck, human_player, computer_player = game_initialize()

        elements = central_deck['activeSize']
        pop_multiple_elements(central_deck['deck'],
                              central_deck['active'],
                              elements)

        pop_multiple_elements(human_player.deck,
                              human_player.hand,
                              human_player.handsize)

        pop_multiple_elements(computer_player.deck,
                              computer_player.hand,
                              computer_player.handsize)

        #create panels
        availiable_cards_panel = Panel(consol.screen, "Available Cards",
                                       "N | Name          Costing        \
Attack           Money", orientation=Orientation.vertical)

        supplement_cards_panel = Panel(consol.screen, "Supplement",
                                       "    Name          Costing        \
Attack           Money", orientation=Orientation.vertical)

        hand_cards_panel = Panel(consol.screen, "Your hand",
                                 "N | Name          Costing        \
Attack           Money", orientation=Orientation.vertical)

        active_cards_panel = Panel(consol.screen, "Active Cards",
                                   "N | Name          Costing        \
Attack           Money", orientation=Orientation.vertical)

        bought_cards_panel = Panel(consol.screen, "Bought Cards",
                                   "N | Name          Costing        \
Attack           Money", orientation=Orientation.vertical)

#        status_panel = Panel(consol.screen, "Game Status",
#                             ["Player:   ", "Computer: "],
#                             Orientation.horizontal)

        status_panel_before_computer_turn = Panel(consol.screen,
                                                  "Game Status Before Computers Turn",
                                                  ["Player:   ", "Computer: "],
                                                  Orientation.horizontal)

        status_money_attack_panel = Panel(consol.screen, "Player Resorses",
                                          ["Money:   ", "Attack:  "],
                                          Orientation.horizontal)

        #set availiable cards Panel and the supplement panel
        availiable_cards_panel.set_message(central_deck['active'])

        if len(central_deck['supplement']) > 0:
            supplement_cards_panel.set_message([central_deck['supplement'][0]])

        #set user options
        consol.read_input("Do you want an aggressive opponent or an acquisative opponent?",
                          ['Aggressive', 'Acquisitive', 'Exit Game'])

        #display the screen
        selection = consol.draw_consol([availiable_cards_panel,
                                        supplement_cards_panel],
                                        right_panel=None, health_status=None)

        #set game difficulty
        if selection == 1:
            computer_player.set_strategy(GameDifficulty.aggressive)
        elif selection == 2:
            computer_player.set_strategy(GameDifficulty.acquisitive)
        elif selection == 3:
            return

        # while play_the_game is True the player is plaing the game
        while play_the_game:
            while True:
                # Set player options
                option_list = []
                for index, card in enumerate(human_player.hand):
                    option_list.append("Select " + card.name +
                                       " card(" + str(index) + ")")

                option_list.append("Play all")
                option_list.append("Buy Card")
                option_list.append("Attack")
                option_list.append("End turn")
                option_list.append("Exit game")
                consol.read_input("Choose Action:", option_list)

                availiable_cards_panel.set_message(central_deck['active'])
                if len(central_deck['supplement']) > 0:
                    supplement_cards_panel.set_message([central_deck['supplement'][0]])

                # set Panels
                hand_cards_panel.set_message(human_player.hand)
                active_cards_panel.set_message(human_player.active)
                status_money_attack_panel.set_message([human_player.money,
                                                       human_player.attack])

                # take user selection
                select = consol.draw_consol([availiable_cards_panel,
                                             supplement_cards_panel,
                                             hand_cards_panel,
                                             active_cards_panel,
                                             status_money_attack_panel], \
                    health_status=[human_player.health, computer_player.health])

                card_index = len(human_player.hand)+1
                play_index = card_index
                buy_card_index = play_index + 1
                attack_index = buy_card_index + 1
                end_index = attack_index + 1
                exit_index = end_index + 1

                # execute user selection actions
                if select < card_index:
                    index = int(select) - 1
                    human_player.take_cart(index, index)
                elif select == play_index:
                    human_player.take_cart(0, len(human_player.hand)-1)
                elif select == buy_card_index:
                    human_player.buy_card(consol, bought_cards_panel,
                                          availiable_cards_panel,
                                          supplement_cards_panel,
                                          status_money_attack_panel, central_deck)
                elif select == attack_index:
                    human_player.attack_operation(computer_player)
                elif select == end_index:
                    human_player.end_turn()
                    break
                elif select == exit_index:
                    return

#            computer_player.money = 0    #check if I can remove that

            status_panel_before_computer_turn.set_message([human_player.health,\
                                                       computer_player.health])

            # play all available cards
            computer_player.take_cart(0, len(computer_player.hand)-1)

            # set Panels at computer turn
            status_money_attack_panel.set_message([computer_player.money, computer_player.attack])
            active_cards_panel.set_message(list(computer_player.active))
            hand_cards_panel.set_message(computer_player.hand)

            # set supplement card field
            if len(central_deck['supplement']) > 0:
                supplement_cards_panel.set_message([central_deck['supplement'][0]])

            # computer attacks
            computer_player.attack_operation(human_player)

            if computer_player.money > 0:
                computer_player.boughtCards = []

                while True:
                    #buy cards
                    if not computer_player.auto_buy_cards(central_deck, 0):
                        break

                # add card at bought_cards_panel
                bought_cards_panel.set_message(computer_player.boughtCards)

            # computer end its turn
            computer_player.end_turn()

            # write all changes that have been during computer turn on screen
            consol.read_input("Press Enter to Continue", [])
            selection = consol.draw_consol([availiable_cards_panel,
                                            supplement_cards_panel,
                                            active_cards_panel,
                                            bought_cards_panel],
                                           [status_panel_before_computer_turn,
                                            status_money_attack_panel],
                                           health_status=[human_player.health,\
                                                       computer_player.health])

            # check if there is a winner
            result = check_for_win(human_player, computer_player, central_deck)
            play_the_game = (result is None)

        # When the game comes to an end, ask the payer if he wants to play
        # again
        consol.read_input("Do you want to play a again?", ["Yes", "No"])
        status_panel_winning = Panel(consol.screen, "End of Game", ["Player:   ",
                                                                    "Computer: ",
                                                                    "Winner: "],
                                     Orientation.horizontal)

        status_panel_winning.set_message([human_player.health, computer_player.health, result])

        selection = consol.draw_consol([status_panel_winning],
                                       right_panel=None, health_status=None)

        play_the_game = (selection == 1)

def ask_user_backround_color():
    print "How is your backround britness?"

    while True:
        response = raw_input("Select:\none (1) for dark\ntwo (2) for white\n\
three (3) for exit\n")

        if response is '1':
            return "bl"
        elif response is '2':
            return "wh"
        elif response is '3':
            exit(0)

def usage(program_name):
    """ Printing the program usage on screen
    """
    print "Usage: %s [OPTION]" % program_name
    print "-b, --brightnes=   set your screen brightnes level"
    print "%-19s if your using dark terminal backround choose [black] or [bl]" % ""
    print "%-19s if your using bright terminal backround choose [white] or [wh]" % ""

def color_opt_usage(program_name):
    """ Printing the color usage on screen
    """
    print "%s: use -b with double dashes as --b [brightnes]" % program_name

if __name__ == '__main__':
    program_name = sys.argv[0]

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hb", ["help", "brightnes="])
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(-2)

    select_brightnes = None
    for option, arg in opts:
        if option in ("-h", "--help"):
            usage(sys.argv[0])
            sys.exit(0)
        elif option in ("-b", "--brightnes"):
            if arg is '':
                color_opt_usage(program_name)
                print "%s: Try '%s --help' for more information." % (program_name, program_name)
                sys.exit(-3)
            select_brightnes = arg
        else:
            print "%s: missing operand" % program_name
            usage(program_name)
            sys.exit(-3)

    if select_brightnes is None:
        select_brightnes = ask_user_backround_color();

    try:
        main(select_brightnes)
    except KeyboardInterrupt:
        try:
            sys.exit(0)
        except SystemExit:
            os.system('reset')
            exit(0)
    os.system('reset')
    exit(0)

